﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte.src.Models
{
    public class RecorridoModel
    {
        public int Id { get; set; }
        public string CodigoRecorrido { get; set; }
        public string NumeroBus { get; set; }
        public string Descripcion { get; set; }
        public int TiempoDemora { get; set; }
        public string ZonaRecorrido { get; set; }
        public int CantidadRecorridos { get; set; }
        public Boolean Estado { get; set; }

    }

}
