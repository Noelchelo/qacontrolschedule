﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte.src.Models
{
    public class SocioModel
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string CodigoSocio { get; set; }
        
        public string Apellidos { get; set; }
        
        public string LineaSocio { get; set; }

        public int CapacidadBus { get; set; }

        public string NumeroBus { get; set; }

        public string Ayudante { get; set; }

        public string Chofer { get; set; }

        public bool Estado { get; set; }
    }
}
