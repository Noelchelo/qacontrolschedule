﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte.src.Models
{
    public class HorarioModel
    {
        public int Id { get; set; }
        public string CodigoHorario { get; set; }
        public string NumeroBus { get; set; }
        public string Descripcion { get; set; }
        public string TiempoSalida { get; set; }
        public string TiempoLlegada { get; set; }
        public bool Estado { get; set; }
    }
}
