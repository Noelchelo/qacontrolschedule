﻿using SistemaControlTransporte.src.DAL;
using SistemaControlTransporte.src.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SistemaControlTransporte.src.BOL
{

    public class SociosBOL
    {
        SociosDAL sdal;
        SocioModel socio;

        /// <summary>
        /// Requisito 11
        /// Crear Socio Correctamente agregando ciertas Validaciones
        /// </summary>
        /// <param name="codigo">string</param>
        /// <returns>ArrayList con el mensaje en string y el recorrido encontrado</returns>
        public ArrayList CrearSocio(SocioModel socio)
        {
            sdal = new SociosDAL();
            ArrayList response = new ArrayList();
            string nulos = AtributosNulos(socio);
            string caracteres = AtributosConCaracteresEspeciales(socio);
            if (string.IsNullOrEmpty(nulos))
            {
                if (string.IsNullOrEmpty(caracteres))
                {
                    string resultado = sdal.CreateSocio(socio);
                    response.Add(resultado);
                    return response;
                }
                else 
                {
                    response.Add(caracteres);
                    return response;
                }
            }
            else 
            {
                response.Add(nulos);
                return response;
            }
        }



        private string AtributosConCaracteresEspeciales(SocioModel socio)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

            string mensaje = "";
            if (!regexItem.IsMatch(socio.CodigoSocio)) { mensaje = "El Codigo del socio Contiene Caracteres Especiales"; }
            if (!regexItem.IsMatch(socio.Nombre)) { mensaje = "El Nombre del socio Contiene Caracteres Especiales"; }
            if (!regexItem.IsMatch(socio.Apellidos)) { mensaje = "Los Apellidos del socio Contienen Caracteres Especiales"; }
            if (!regexItem.IsMatch(socio.NumeroBus)) { mensaje = "El Numero de Bus del socio Contienen Caracteres Especiales"; }
            if (!regexItem.IsMatch(socio.LineaSocio)) { mensaje = "La Linea del socio Contiene Caracteres Especiales"; }
            if (socio.CapacidadBus <= 0) { mensaje = "La Capacidad de bus es menor o igual a 0"; }
            if (!regexItem.IsMatch(socio.Ayudante)) { mensaje = "El Ayudante del socio Contiene Caracteres Especiales"; }
            if (!regexItem.IsMatch(socio.Chofer)) { mensaje = "El Chofer del socio Contiene Caracteres Especiales"; }
            return mensaje;
        }


        /// <summary>
        /// Revisa los Atributos del socio para validar que vengan llenos
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        private string AtributosNulos(SocioModel socio)
        {
            string mensaje = "";
            if (socio==null){return "El socio es un objecto Nulo";}
            if (string.IsNullOrEmpty(socio.CodigoSocio)) { mensaje = "El Codigo del socio Viene Vacio o Nulo"; }
            if (string.IsNullOrEmpty(socio.Nombre)) { mensaje = "El Nombre del socio Viene Vacio o Nulo"; }
            if (string.IsNullOrEmpty(socio.Apellidos)) { mensaje = "Los Apellidos del socio Viene Vacio o Nulo"; }
            if (string.IsNullOrEmpty(socio.LineaSocio)) { mensaje = "La Linea del socio Viene Vacio o Nulo"; }
            if (string.IsNullOrEmpty(socio.NumeroBus)) { mensaje = "El Numero de bus del socio Viene Vacio o Nulo"; }
            if (socio.CapacidadBus<=0) { mensaje = "La Capacidad de bus es menor o igual a 0 o Nulo"; }
            if (string.IsNullOrEmpty(socio.Ayudante)) { mensaje = "El Ayudante del socio Viene Vacio o Nulo"; }
            if (string.IsNullOrEmpty(socio.Chofer)) { mensaje = "El Chofer del socio Viene Vacio o Nulo"; }
            return mensaje;
        }
    }
}
