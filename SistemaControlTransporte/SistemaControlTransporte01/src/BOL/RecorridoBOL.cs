﻿using SistemaControlTransporte.src.DAL;
using SistemaControlTransporte.src.Models;
using SistemaControlTransporte01.src;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SistemaControlTransporte.src.BOL
{
    public class RecorridoBOL
    {
        RecorridoDAL rdal;
        RecorridoModel recorrido;
        public IServices IServicioRecorrido;

        public RecorridoBOL(IServices iServicioHorario)
        {
            IServicioRecorrido = iServicioHorario;
        }

        public RecorridoBOL()
        {
        }

        /// <summary>
        /// Requisito 06
        /// Crea un recorido
        /// </summary>
        /// <param name="Recorrido">objeto de recorrido</param>
        /// <returns>retorna un array con la respuesta de la ejecucion</returns>
        public ArrayList CrearRecorrido(RecorridoModel Recorrido)
        {
            rdal = new RecorridoDAL();
            ArrayList response = new ArrayList();
            var RServices = IServicioRecorrido.RevisarRecorrido(Recorrido);
            if(RServices[0].ToString() == "Datos Correctos")
            {
                rdal.CrearRecorrido(Recorrido);
                response.Add("Realizado");
                return response;
            }
            if (RServices[0].ToString() == "Numero de bus no existe")
            {
                response.Add("El numero de bus no esta en la base de datos");
                return response;
            }
            else
            {
                response.Add(RServices[0].ToString());
            }
            
            return response;
        }

        /// <summary>
        /// Requisito 07
        /// Busca un recorrido mediante el código
        /// </summary>
        /// <param name="codigo">string</param>
        /// <returns>ArrayList con el mensaje en string y el recorrido encontrado</returns>
        public ArrayList BuscarRecorrido(string codigo)
        {
            rdal = new RecorridoDAL();
            ArrayList response = new ArrayList();

            if (String.IsNullOrEmpty(codigo))
            {

                response.Add("No se aceptan espacios en blanco o valores nulos para la busqueda de recorridos.");
                return response;
            }
            else
            {
                string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
                foreach (var item in specialChar)
                {
                    if (codigo.Contains(item))
                    {
                        response.Add("No se aceptan caracteres especiales en el codigo, solo números y letras.");
                        return response;
                    }
                }
                codigo.Trim();
                recorrido = rdal.BuscarRecorrido(codigo);
                if (recorrido == null)
                {
                    response.Add("No se encontró ningún recorrido con el id proporcionado.");
                    return response;
                }
                else
                {
                    response.Add("Se encontró un recorrido.");
                    response.Add(recorrido);
                    return response;
                }

            }


        }


        /// <summary>
        /// Requisito 08
        /// Actualiza los datos de un recorrido 
        /// buscado mediante el metodo BuscarRecorrido
        /// </summary>
        /// <param name="r">Objeto RecorridoModel</param>
        /// <returns>string con un mensaje de exito o error</returns>
        public string ActualizarRecorrido(RecorridoModel r)
        {
            rdal = new RecorridoDAL();
            if (String.IsNullOrEmpty(r.NumeroBus.Trim(' ')))
            {
                return "Es necesario asignar un número de bus, no se aceptan espacios en blanco o valores nulos.";
            }
            else
            {
                if (String.IsNullOrEmpty(r.CodigoRecorrido.Trim(' ')))
                {
                    return "Se necesita un código de recorrido.";
                }
                else
                {

                    if (!rdal.ActualizarRecorrido(r))
                    {
                        return "Error al actualizar el recorrido, intente de nuevo por favor.";
                    }
                    else
                    {
                        return "Recorrido actualizado de manera exitosa.";
                    }

                }
            }

        }

        /// <summary>
        /// Requisito 09
        /// Elimina un recorrido registrado mediante el
        /// codigo de recorrido
        /// </summary>
        /// <param name="codigo">String</param>
        /// <returns>string con un mensaje de exito o error</returns>
        public string EliminarRecorrido(string codigo)
        {
            rdal = new RecorridoDAL();
            if (String.IsNullOrEmpty(codigo.Trim(' ')))
            {
                return "Se necesita un código de recorrido, no se aceptan espacios en blanco o valores nulos.";
            }
            else
            {
                if (rdal.EliminarRecorrido(codigo))
                {
                    return "El recorrido se eliminó de manera exitosa.";
                }
                else
                {
                    return "El código de recorrido proporcionado no pertenece a ningún recorrido existente.";
                }

            }

        }

        /// <summary>
        /// Requisito 10
        /// Busca en la Base de Datos un reccorrido un un CODIGO especifico
        /// El CODIGO tiene que pasar por ciertas validaciones antes de generar el reporte.
        /// </summary>
        /// <param name="codigo">CODIGO: Codigo del Recorrido que se busca generar un reporte.</param>
        /// <returns></returns>
        public ArrayList ReporteRecorrido(string codigo)
        {
            rdal = new RecorridoDAL();
            ArrayList response = new ArrayList();
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            if (String.IsNullOrEmpty(codigo.Trim(' ')))
            {
                response.Add("Es necesario asignar numero de ruta, no se aceptan espacios en blanco o valores nulos.");
                return response;
            }
            else if (!regexItem.IsMatch(codigo))
            {
                response.Add("No se aceptan caracteres especiales en el codigo, solo números y letras.");
                return response;
            }
            else 
            {
                recorrido = rdal.BuscarRecorrido(codigo);
                if (recorrido == null)
                {
                    response.Add("No se encontró ningún recorrido con el id proporcionado.");
                    return response;
                }
                else
                {
                    response.Add("Se ha producido el reporte.");
                    response.Add(recorrido);
                    return response;
                }
            }
        }
    }
}
