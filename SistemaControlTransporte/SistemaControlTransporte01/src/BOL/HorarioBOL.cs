﻿using SistemaControlTransporte.src.DAL;
using SistemaControlTransporte.src.Models;
using SistemaControlTransporte01.src;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace SistemaControlTransporte.src.BOL
{
    public class HorarioBOL
    {
       public HorarioDAL hdal;
        public IServices IServicioHorario;

        public HorarioBOL(IServices IServicioHorario)
        {
            this.IServicioHorario = IServicioHorario;
        }

        public HorarioBOL()
        {
        }


        /// <summary>
        /// Requisito 01
        /// Crear un Horario correctamente agregando ciertas validaciones
        /// </summary>
        /// <param name="horario">string</param>
        /// <returns>ArrayList con el mensaje en string si se creo el horario o no es valido</returns>
        public ArrayList CrearHorario(HorarioModel horario)
        {
            hdal = new HorarioDAL();
            ArrayList response = new ArrayList();
            string nulos = AtributosNulos(horario);
            if (string.IsNullOrEmpty(nulos))
            {
                string caracteres = AtributosConCaracteresEspeciales(horario);
                if (string.IsNullOrEmpty(caracteres))
                {
                    string resultado = hdal.CrearHorario(horario);
                    response.Add(resultado);
                    return response;
                }
                else
                {
                    response.Add(caracteres);
                    return response;
                }
            }
            else
            {
                response.Add(nulos);
                return response;
            }
        }
        /// <summary>
        /// Requisito 02
        /// Busca un horario por el código horario
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns>ArrayList con el mensaje en string con el horario encontrado o no</returns>
        public ArrayList BuscarHorarioPorCodigoHorario(string codigo) 
        {
            hdal = new HorarioDAL();
            ArrayList response = new ArrayList();
            string validar = ValidarTexto(codigo);
            if (string.IsNullOrEmpty(validar)) 
            {
                HorarioModel horario = hdal.ObtenerHorarioPorCodigoHorario(codigo);
                if(horario != null) 
                {
                    response.Add("Se encontró el horario");
                }
                else
                {
                    response.Add("No se encontró ningún horario");
                }
                return response;
            }
            else 
            {
                response.Add(validar);
                return response;
            }
        }

        /// <summary>
        /// Requisito 03
        /// Actualiza el horario utilizando el código del horario
        /// </summary>
        /// <param name="horario"></param>
        /// <returns>ArrayList con el mensaje en string si se actualizo el horario o no es valido</returns>
        public ArrayList ActualizarHorario(HorarioModel horario)
        {
            hdal = new HorarioDAL();
            ArrayList response = new ArrayList();
            string nulos = AtributosNulos(horario);
            if (string.IsNullOrEmpty(nulos))
            {
                string caracteres = AtributosConCaracteresEspeciales(horario);
                if (string.IsNullOrEmpty(caracteres))
                {
                    bool resultado = hdal.ActualizarHorario(horario);
                    if (resultado) { response.Add("Se actualizó el horario"); }
                    else {response.Add("No se actualizó el horario"); }
                    return response;
                }
                else
                {
                    response.Add(caracteres);
                    return response;
                }
            }
            else
            {
                response.Add(nulos);
                return response;
            }
        }

        /// <summary>
        /// Requisito 04
        /// Desactiva de manera logica el numero de Horario
        /// </summary>
        /// <param name="Codigo">Codigo del horario</param>
        /// <returns>ArrayList con el mensaje en string para el resultado del metodo</returns>
        public ArrayList EliminarHorario(String Codigo)
        {
            ArrayList response = new ArrayList();
            if(IServicioHorario.Nulo(Codigo))
            {
                //Busqueda del horario por el requisito 2, metodo llamado BuscarHorarioPorCodigoHorario
                ArrayList RespuestaBusquedaHorario = BuscarHorarioPorCodigoHorario(Codigo);

                if (RespuestaBusquedaHorario[0].ToString() == "Se encontró el horario")
                {
                    HorarioDAL Dal = new HorarioDAL();
                    Dal.EliminarHorario(Codigo);
                    response.Add("Eliminación de horario con exito");
                    return response;
                }
                else
                {
                    response.Add("Horario no existe");
                    return response;
                }
                
            }
            else
            {
                response.Add("Codigo esta en nulo");
            }
            return response;
        }

        /// <summary>
        /// Requisito 05
        /// Genera objeto ModelHorario para agregarlo al reporte
        /// </summary>
        /// <param name="Codigo">Codigo del horario a buscar</param>
        /// <returns>Un array con un item del modelo de horario con los datos para generar el reporte sino 
        /// retorna un array con el error procedente</returns>
        public ArrayList Reporte(String Codigo)
        {
            ArrayList response = new ArrayList();

            string validar = ValidarTexto(Codigo);
            if (string.IsNullOrEmpty(validar))
            {
                //Busqueda del horario por el requisito 2, metodo llamado BuscarHorarioPorCodigoHorario
                ArrayList RespuestaBusquedaHorario = BuscarHorarioPorCodigoHorario(Codigo);

                if (RespuestaBusquedaHorario[0].ToString() == "Se encontró el horario")
                {
                    HorarioModel horario = hdal.ObtenerHorarioPorCodigoHorario(Codigo);
                    response.Add(horario);
                }
                else
                {
                    response.Add("Codigo no existe");
                    return response;
                }
            }
            else
            {
                response.Add(validar);
                
            }
            return response;
        }


        /// <summary>
        /// Verifica si un atributo del modelo horario tiene caracteres especiales
        /// </summary>
        /// <param name="horario"></param>
        /// <returns>string con el mensaje si tiene caracteres especiales </returns>
        private string AtributosConCaracteresEspeciales(HorarioModel horario)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

            string mensaje = "";
            if(!regexItem.IsMatch(horario.CodigoHorario)) { mensaje = "El código del horario contiene caracteres especiales"; }
            if (!regexItem.IsMatch(horario.NumeroBus)) { mensaje = "El número de bus contiene caracteres especiales"; }
            return mensaje;
        }

        /// <summary>
        /// Verifica si los atributos del modelo horario son nulos
        /// </summary>
        /// <param name="horario"></param>
        /// <returns>string con el mensaje si tiene un atributo nulo</returns>
        private string AtributosNulos (HorarioModel horario)
        {
            string mensaje = "";
            if (horario == null) { return "El horario es un objeto nulo"; }
            if (string.IsNullOrEmpty(horario.CodigoHorario)) { return "El codigo del horario es nulo o vacio"; }
            if (string.IsNullOrEmpty(horario.NumeroBus)) { return "El número de bus es nulo o vacío"; }
            if (string.IsNullOrEmpty(horario.TiempoSalida)) { return "El tiempo de salida es nulo o vacío"; }

            return mensaje;
        }

        /// <summary>
        /// Verifica si un string es invalido
        /// </summary>
        /// <param name="texto"></param>
        /// <returns>string con mensaje si es invalido</returns>
        private string ValidarTexto (string texto) 
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            string mensaje = "";
            if (string.IsNullOrEmpty(texto)) { return "No puede contener espacios en blanco o nulos"; }
            if (!regexItem.IsMatch(texto)) { return "El código del horario contiene caracteres especiales"; }
            return mensaje;
        }
    }
}
