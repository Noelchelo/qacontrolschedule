﻿using Npgsql;
using SistemaControlTransporte.src.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte.src.DAL
{
    public class SociosDAL
    {
        /// <summary>
        /// Requisito 11
        /// Metodo de insercion a la base de datos (Socios)
        /// </summary>
        /// <param name="codigo">string</param>
        /// <returns>Object RecorridoModel</returns>
        public string CreateSocio(SocioModel socio)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir una consulta
                string sql = @"INSERT INTO public.socios(codigo_socio, nombre, apellidos, linea_socio, numero_bus, capacidad_bus, ayudante, chofer, estado)VALUES (@codigo_socio, @nombre, @apellidos, @linea_socio, @numero_bus, @capacidad_bus, @ayudante, @chofer, @estado)returning codigo_socio";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo_socio", socio.CodigoSocio);
                cmd.Parameters.AddWithValue("@nombre", socio.Nombre);
                cmd.Parameters.AddWithValue("@apellidos", socio.Apellidos);
                cmd.Parameters.AddWithValue("@linea_socio", socio.LineaSocio);
                cmd.Parameters.AddWithValue("@numero_bus", socio.NumeroBus);
                cmd.Parameters.AddWithValue("@capacidad_bus", socio.CapacidadBus);
                cmd.Parameters.AddWithValue("@ayudante", socio.Ayudante);
                cmd.Parameters.AddWithValue("@chofer", socio.Chofer);
                cmd.Parameters.AddWithValue("@estado", socio.Estado);
                string idPar = cmd.ExecuteScalar().ToString();
                if (!string.IsNullOrEmpty(idPar))
                {
                    return "Se guardo el Socio Correctamente";
                }
                else 
                {
                    return "No se guardo el Socio correctamente";
                }
            }
        }

        private SocioModel CrearSocio(NpgsqlDataReader reader)
        {
            SocioModel r = new SocioModel();
            r.Id = Convert.ToInt32(reader["id"]);
            r.CodigoSocio = reader["codigo_socio"].ToString();
            r.Apellidos = reader["apellidos "].ToString();
            r.LineaSocio = reader["linea_socio "].ToString();
            r.NumeroBus = reader["numero_bus "].ToString();
            r.CapacidadBus = Convert.ToInt32(reader["capacidad_bus"]);
            r.Ayudante = reader["ayudante"].ToString();
            r.Chofer = reader["chofer"].ToString();
            r.Estado = Convert.ToBoolean(reader["estado"]);
            return r;
        }

        /// <summary>
        /// Requisito 06
        /// Verifica si el numero de bus existe
        /// </summary>
        /// <param name="codigo">string</param>
        /// <returns>boolean</returns>
        public Boolean VerificarSiExisteNumeroDeBus(string codigo)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexión
                con.Open();
                //Definir una consulta
                string sql = @"SELECT id FROM socios WHERE numero_bus = @codigo";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return true;
                }

            }
            return false;
        }

    }

    

}
