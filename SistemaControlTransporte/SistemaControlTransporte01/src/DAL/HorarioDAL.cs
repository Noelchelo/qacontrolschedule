﻿using Microsoft.VisualBasic.CompilerServices;
using Npgsql;
using SistemaControlTransporte.src.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte.src.DAL
{
    public class HorarioDAL
    {
        /// <summary>
        /// Requisito 01
        /// Crear un nuevo horario y lo inserta en la base de datos.
        /// </summary>
        /// <param name="horario">string</param>
        /// <returns> string si fue creado o no</returns>
        public string CrearHorario(HorarioModel horario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexión
                con.Open();
                //Definir una consulta
                string sql = @"INSERT INTO public.horarios(codigo_horario, numero_bus, descripcion, tiempo_salida, tiempo_llegada, estado) VALUES (@codigo_horario, @numero_bus, @descripcion, @tiempo_salida, @tiempo_llegada, @estado) returning codigo_horario";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo_horario", horario.CodigoHorario);
                cmd.Parameters.AddWithValue("@numero_bus", horario.NumeroBus);
                cmd.Parameters.AddWithValue("@descripcion", horario.Descripcion);
                cmd.Parameters.AddWithValue("@tiempo_salida", horario.TiempoSalida);
                cmd.Parameters.AddWithValue("@tiempo_llegada", horario.TiempoLlegada);
                cmd.Parameters.AddWithValue("@estado", horario.Estado);
                string idPar = cmd.ExecuteScalar().ToString();
                if (!string.IsNullOrEmpty(idPar))
                {
                    return "Se guardó el horario correctamente";
                }
                else
                {
                    return "No se guardó el horario correctamente";
                }
            }
        }

        /// <summary>
        /// Requisito 02
        /// Busca un horario por el código horario y lo retorna
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns>HorarioModel</returns>
        public HorarioModel ObtenerHorarioPorCodigoHorario(string codigo) 
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexión
                con.Open();
                //Definir una consulta
                string sql = @"SELECT id, codigo_horario, numero_bus, descripcion, tiempo_salida, tiempo_llegada, estado FROM horarios WHERE codigo_horario = @codigo";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CrearHorario(reader);
                }
                return null;
            }
        }

        /// <summary>
        /// Requisito 03
        /// Actualiza un horario con el código de horario
        /// </summary>
        /// <param name="horario"></param>
        /// <returns>Bool true si se actualizó, false si no</returns>
        public bool ActualizarHorario(HorarioModel horario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexión
                con.Open();
                //Definir una consulta
                string sql = @"UPDATE public.horarios SET  numero_bus=@numero_bus, 
                descripcion=@descripcion,tiempo_salida=@tiempo_salida, tiempo_llegada=@tiempo_llegada, 
                estado=@estado WHERE  codigo_horario = @codigo_horario;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo_horario", horario.CodigoHorario);
                cmd.Parameters.AddWithValue("@numero_bus", horario.NumeroBus);
                cmd.Parameters.AddWithValue("@descripcion", horario.Descripcion);
                cmd.Parameters.AddWithValue("@tiempo_salida", horario.TiempoSalida);
                cmd.Parameters.AddWithValue("@tiempo_llegada", horario.TiempoLlegada);
                cmd.Parameters.AddWithValue("@estado", horario.Estado);
                if (cmd.ExecuteNonQuery() != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Requisito 04
        /// Elimina de manera logica un horario
        /// </summary>
        /// <param name="horario"></param>
        /// <returns>Bool true si se actualizó, false si no</returns>
        ///
        public bool EliminarHorario(string horario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexión
                con.Open();
                //Definir una consulta
                string sql = @"DELETE FROM horarios 
                WHERE  codigo_horario = @codigo_horario;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo_horario", horario);
                
                if (cmd.ExecuteNonQuery() != -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Crea un objeto HorarioModel 
        /// sacando los datos de un NpgsqlDataReader
        /// </summary>
        /// <param name="reader">NpgsqlDataReader</param>
        /// <returns>Object HorarioModel </returns>
        private HorarioModel CrearHorario(NpgsqlDataReader reader)
        {
            HorarioModel h = new HorarioModel();
            h.Id = Convert.ToInt32(reader["id"]);
            h.CodigoHorario = reader["codigo_horario"].ToString();
            h.NumeroBus = reader["numero_bus"].ToString();
            h.Descripcion = reader["descripcion"].ToString();
            h.TiempoSalida = reader["tiempo_salida"].ToString();
            h.TiempoLlegada = reader["tiempo_llegada"].ToString();
            h.Estado = Convert.ToBoolean(reader["estado"]);
            return h;
        }

    }
}
