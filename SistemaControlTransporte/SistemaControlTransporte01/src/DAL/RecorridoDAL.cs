﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
using SistemaControlTransporte.src.Models;

namespace SistemaControlTransporte.src.DAL
{
    public class RecorridoDAL
    {
        /// <summary>
        /// Requisito 06
        /// Crea un nuevo recorrido
        /// </summary>
        /// <param name="Recorrido">RecorridoModel</param>
        /// <returns>Si se creo o no el recorrido</returns>
        public string CrearRecorrido(RecorridoModel Recorrido)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexión
                con.Open();
                //Definir una consulta
                string sql = @"INSERT INTO public.recorridos(codigo_recorrido, numero_bus, descripcion, tiempo_demora, zona_recorrido, cantidad_recorridos, estado) 
                VALUES (@codigo_recorrido, @numero_bus, @descripcion, @tiempo_demora, @zona_recorrido,@cantidad_recorridos, @estado) returning codigo_recorrido";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo_recorrido", Recorrido.CodigoRecorrido);
                cmd.Parameters.AddWithValue("@numero_bus", Recorrido.NumeroBus);
                cmd.Parameters.AddWithValue("@descripcion", Recorrido.Descripcion);
                cmd.Parameters.AddWithValue("@tiempo_demora", Recorrido.TiempoDemora);
                cmd.Parameters.AddWithValue("@zona_recorrido", Recorrido.ZonaRecorrido);
                cmd.Parameters.AddWithValue("@cantidad_recorridos", Recorrido.CantidadRecorridos);
                cmd.Parameters.AddWithValue("@estado", Recorrido.Estado);
                string idPar = cmd.ExecuteScalar().ToString();
                if (!string.IsNullOrEmpty(idPar))
                {
                    return "Se guardó el recorrido correctamente";
                }
                else
                {
                    return "No se guardó el recorrido correctamente";
                }
            }
        }

        /// <summary>
        /// Requisito 07
        /// Busca un recorrido mediante el código de recorrido en la BD
        /// </summary>
        /// <param name="codigo">string</param>
        /// <returns>Object RecorridoModel</returns>
        public RecorridoModel BuscarRecorrido(string codigo)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir una consulta
                string sql = @"SELECT id, codigo_recorrido, numero_bus, descripcion, tiempo_demora, zona_recorrido, cantidad_recorridos, estado
	            FROM recorridos WHERE codigo_recorrido = @codigo";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CrearRecorrido(reader);
                }
                return null;
                

            }
        }

        
        /// <summary>
        /// Requisito 08
        /// Actualiza los datos de un recorrido 
        /// en la BD
        /// </summary>
        /// <param name="recorrido">Objeto RecorridoModel</param>
        /// <returns>True si la actualización fue exitosa y false si no lo fue</returns>
        public bool ActualizarRecorrido(RecorridoModel recorrido) 
        {
                using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
                {
                    //Abrir una conexion
                    con.Open();

                    //Definir una consulta
                    string sql = @"UPDATE public.recorridos SET  numero_bus=@numero_bus, 
                descripcion=@descripcion,tiempo_demora=@tiempo_demora, zona_recorrido=@zona_recorrido, 
                cantidad_recorridos=@cantidad_recorridos,  estado=@estado WHERE  codigo_recorrido = @codigo_recorrido;";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@numero_bus", recorrido.NumeroBus);
                    cmd.Parameters.AddWithValue("@descripcion", recorrido.Descripcion);
                    cmd.Parameters.AddWithValue("@tiempo_demora", recorrido.TiempoDemora);
                    cmd.Parameters.AddWithValue("@zona_recorrido", recorrido.ZonaRecorrido);
                    cmd.Parameters.AddWithValue("@cantidad_recorridos", recorrido.CantidadRecorridos);
                    cmd.Parameters.AddWithValue("@estado", recorrido.Estado);
                    cmd.Parameters.AddWithValue("@codigo_recorrido", recorrido.CodigoRecorrido);
                    if (cmd.ExecuteNonQuery() == -1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
        }


        /// <summary>
        /// Requisito 09
        /// Elimina un recorrido registrado mediante el
        /// codigo de recorrido en la BD
        /// </summary>
        /// <param name="codigo">String</param>
        /// <returns>True si la eliminación fue exitosa y false si no lo fue</returns>
        public bool EliminarRecorrido(String codigo)
        {

            using (NpgsqlConnection con = new NpgsqlConnection(conexion.ConStr))
            {
                //Abrir una conexion
                con.Open();

                //Definir una consulta
                string sql = @"DELETE FROM recorridos WHERE codigo_recorrido = @codigo_recorrido;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@codigo_recorrido", codigo);

                if (cmd.ExecuteNonQuery() == 0)
                {
                    return false;
                }
                else {
                    return true;
                }

                



            }
        }




        /// <summary>
        /// Crea un objeto RecorridoModel 
        /// sacando los datos de un NpgsqlDataReader
        /// </summary>
        /// <param name="reader">NpgsqlDataReader</param>
        /// <returns>Object RecorridoModel</returns>
        private RecorridoModel CrearRecorrido(NpgsqlDataReader reader)
        {
            RecorridoModel r = new RecorridoModel();
            r.Id = Convert.ToInt32(reader["id"]);
            r.CodigoRecorrido= reader["codigo_recorrido"].ToString();
            r.NumeroBus = reader["numero_bus"].ToString();
            r.Descripcion = reader["descripcion"].ToString();
            r.TiempoDemora = Convert.ToInt32(reader["tiempo_demora"]);
            r.ZonaRecorrido = reader["zona_recorrido"].ToString();
            r.CantidadRecorridos = Convert.ToInt32(reader["cantidad_recorridos"]);
            r.Estado = Convert.ToBoolean(reader["estado"]);
            return r;
        }
    }
}
