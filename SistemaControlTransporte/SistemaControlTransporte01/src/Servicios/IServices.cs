﻿using SistemaControlTransporte.src.DAL;
using SistemaControlTransporte.src.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte01.src
{
    public interface IServices
    {

        /// <summary>
        /// Requisito 04
        /// Elimina el horario de DB
        /// </summary>
        /// <param name="Codigo">string</param>
        /// <returns>Un array con la informacion de la respuesta</returns>
        public ArrayList Resultado(String Codigo)
        {
            HorarioDAL Dal = new HorarioDAL();
            ArrayList response = new ArrayList();

                if (Dal.EliminarHorario(Codigo))
                {
                    response.Add("Eliminación de horario con exito");
                    return response;
                }
                else
                {
                    response.Add("No se pudo eliminar el horario");
                    
                }
            return response;
        }
        /// <summary>
        /// Requisito 04
        /// Procesa la validacion de si el codigo esta nulo
        /// </summary>
        /// <param name="Codigo">string</param>
        /// <returns>Un array con la informacion de la respuesta</returns>
        public Boolean Nulo(String Codigo)
        {
            if (Codigo == "")
            {
                return true;
            }
            else
            {
                return false;

            }
        }

        /// <summary>
        /// Requisito 06
        /// Valida el modelo y los datos del recorrido
        /// </summary>
        /// <param name="Recorrido">RecoridoModal</param>
        /// <returns>Un array con la informacion de la respuesta</returns>
        /// 
        public ArrayList RevisarRecorrido(RecorridoModel Recorrido)
        {
            ArrayList response = new ArrayList();
            SociosDAL Dal = new SociosDAL();
            if (Recorrido.CodigoRecorrido == "" || Recorrido.Descripcion == "" || Recorrido.NumeroBus == ""
                || Recorrido.ZonaRecorrido == "")
            {
                response.Add("Tiene datos vacios");
                return response;
            }
            if (Recorrido.TiempoDemora < 1 || Recorrido.CantidadRecorridos < 0)
            {
                response.Add("Datos invalidos");
                return response;
            }
            if (Dal.VerificarSiExisteNumeroDeBus(Recorrido.NumeroBus) == false)
            {
                response.Add("Numero de bus no existe");
                return response;
            }
            else
            {
                response.Add("Datos Correctos");
            }

            return response;
        }

    }
}
