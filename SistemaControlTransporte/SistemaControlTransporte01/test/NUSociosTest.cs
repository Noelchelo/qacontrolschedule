﻿using NUnit.Framework;
using SistemaControlTransporte.src.BOL;
using SistemaControlTransporte.src.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaControlTransporte01.test
{
    [TestFixture]
    public class NUSociosTest
    {
        /// <summary>
        /// TSC-031
        /// Requisito 11
        /// </summary>
        [Test]
        public void CrearSocioExitoso()
        {
            //Arrange
            SocioModel socio = new SocioModel
            {
                Nombre = "Orlando",
                CodigoSocio = "001ZZZ",
                Apellidos = "Florida Obama",
                LineaSocio = "Buses San Jose",
                CapacidadBus = 20,
                NumeroBus = "ZZA400",
                Ayudante = "Carlitos el Unico",
                Chofer = "Palbito Alfajore",
                Estado = true
            };
            SociosBOL sbol = new SociosBOL();
            string resultaesperado = "Se guardo el Socio Correctamente";
            //Act
            var result = sbol.CrearSocio(socio);
            //Assert
            Assert.AreEqual(resultaesperado, result[0]);
        }

        /// <summary>
        /// TSC-032
        /// Requisito 11
        /// </summary>
        [Test]
        public void CrearSocioApellidoInvalido()
        {
            //Arrange
            SocioModel socio = new SocioModel
            {
                Nombre = "Orlando",
                CodigoSocio = "001ZZZ",
                Apellidos = "@Florida-Obama%",
                LineaSocio = "Buses San Jose",
                CapacidadBus = 20,
                NumeroBus = "ZZA400",
                Ayudante = "Carlitos el Unico",
                Chofer = "Palbito Alfajore",
                Estado = true
            };
            SociosBOL sbol = new SociosBOL();
            string resultaesperado = "Los Apellidos del socio Contienen Caracteres Especiales";
            //Act
            var result = sbol.CrearSocio(socio);
            //Assert
            Assert.AreEqual(resultaesperado, result[0]);
        }
        /// <summary>
        /// TSC-033
        /// Requisito 11
        /// </summary>
        [Test]
        public void CrearSocioNumeroBusNulo()
        {
            //Arrange
            SocioModel socio = new SocioModel
            {
                Nombre = "Orlando",
                CodigoSocio = "001ZZZ",
                Apellidos = "Florida Obama",
                LineaSocio = "Buses San Jose",
                CapacidadBus = 20,
                NumeroBus = "",
                Ayudante = "Carlitos el Unico",
                Chofer = "Palbito Alfajore",
                Estado = true
            };
            SociosBOL sbol = new SociosBOL();
            string resultaesperado = "El Numero de bus del socio Viene Vacio o Nulo";
            //Act
            var result = sbol.CrearSocio(socio);
            //Assert
            Assert.AreEqual(resultaesperado, result[0]);
        }

    }
}