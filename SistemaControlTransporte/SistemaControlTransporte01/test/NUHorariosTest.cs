﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;
using SistemaControlTransporte.src.BOL;
using SistemaControlTransporte.src.Models;
using SistemaControlTransporte01.src;
using Stripe;

namespace SistemaControlTransporte01.test
{
    [TestFixture]
    public class NUHorariosTest
    {
        HorarioBOL hbol;

        /// <summary>
        /// TSC-001
        /// Requisito 01
        /// </summary>
        [Test]
        public void CrearHorarioExitoso()
        {
            //Arrange
            HorarioModel horario = new HorarioModel
            {
                CodigoHorario = "01",
                NumeroBus = "ZZA400",
                Descripcion = "Horarios de la Palmera",
                TiempoSalida = "7:00",
                TiempoLlegada = "8:00",
                Estado = true
            };
            hbol = new HorarioBOL();
            string resultaesperado = "Se guardó el horario correctamente";
            //Act
            var result = hbol.CrearHorario(horario);
            //Assert
            Assert.AreEqual(resultaesperado, result[0]);
        }

        /// <summary>
        /// TSC-002
        /// Requisito 01
        /// </summary>
        [Test]
        public void CrearHorarioNumeroBusVacio()
        {
            //Arrange
            HorarioModel horario = new HorarioModel
            {
                CodigoHorario = "111",
                NumeroBus = "",
                Descripcion = "Horarios Cuidad Quesada",
                TiempoSalida = "8:00",
                TiempoLlegada = "8:30",
                Estado = true
            };
            hbol = new HorarioBOL();
            string resultadoesperado = "El número de bus es nulo o vacío";
            //Act
            var result = hbol.CrearHorario(horario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }

        /// <summary>
        /// TSC-003
        /// Requisito 01
        /// </summary>
        [Test]
        public void CrearHorarioNumeroBusInvalido()
        {
            //Arrange
            HorarioModel horario = new HorarioModel
            {
                CodigoHorario = "665",
                NumeroBus = "12@do-s",
                Descripcion = "Horarios Pital",
                TiempoSalida = "9:00",
                TiempoLlegada = "10:30",
                Estado = true
            };
            hbol = new HorarioBOL();
            string resultadoesperado = "El número de bus contiene caracteres especiales";
            //Act
            var result = hbol.CrearHorario(horario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }

        /// <summary>
        /// TSC-004
        /// Requisito 02
        /// </summary>
        [Test]
        public void BuscarHorarioExitoso()
        {
            //Arrange
            string codigohorario = "01";
            string resultadoesperado = "Se encontró el horario";
            hbol = new HorarioBOL();
            //Act
            var result = hbol.BuscarHorarioPorCodigoHorario(codigohorario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }
        /// <summary>
        /// TSC-005
        /// Requisito 02
        /// </summary>
        [Test]
        public void BuscarHorarioSinExito()
        {
            //Arrange
            string codigohorario = "22";
            string resultadoesperado = "No se encontró ningún horario";
            hbol = new HorarioBOL();
            //Act
            var result = hbol.BuscarHorarioPorCodigoHorario(codigohorario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }
        /// <summary>
        /// TSC-006
        /// Requisito 02
        /// </summary>
        [Test]
        public void BuscarHorarioCaracteresEspeciales()
        {
            //Arrange
            string codigohorario = "45@!";
            string resultadoesperado = "El código del horario contiene caracteres especiales";
            hbol = new HorarioBOL();
            //Act
            var result = hbol.BuscarHorarioPorCodigoHorario(codigohorario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }

        /// <summary>
        /// TSC-007
        /// Requisito 03
        /// </summary>
        [Test]
        public void ActualizarHorarioExitoso()
        {
            //Arrange
            HorarioModel horario = new HorarioModel
            {
                CodigoHorario = "01",
                NumeroBus = "45",
                Descripcion = "Horarios Pital",
                TiempoSalida = "9:00",
                TiempoLlegada = "10:30",
                Estado = true
            };
            string resultadoesperado = "Se actualizó el horario";
            hbol = new HorarioBOL();
            //Act
            var result = hbol.ActualizarHorario(horario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }

        /// <summary>
        /// TSC-008
        /// Requisito 03
        /// </summary>
        [Test]
        public void ActualizarHorarioTiempoSalidaVacio()
        {
            //Arrange
            HorarioModel horario = new HorarioModel
            {
                CodigoHorario = "555",
                NumeroBus = "12",
                Descripcion = "Horarios Pital",
                TiempoSalida = "",
                TiempoLlegada = "10:30",
                Estado = true
            };
            string resultadoesperado = "El tiempo de salida es nulo o vacío";
            hbol = new HorarioBOL();
            //Act
            var result = hbol.ActualizarHorario(horario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }
        /// <summary>
        /// TSC-009
        /// Requisito 03
        /// </summary>
        [Test]
        public void ActualizarHorarioNulo()
        {
            //Arrange
            HorarioModel horario = null;
            string resultadoesperado = "El horario es un objeto nulo";
            hbol = new HorarioBOL();
            //Act
            var result = hbol.ActualizarHorario(horario);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);
        }
        /// <summary>
        /// TSC-010
        /// Requisito 04
        /// </summary>
        [Test]
        public void EliminarHorarioCuandoSeIntroduceUnCodigoIncorrecto()
        {
            //Arrange
            String Codigo = "hbd5ew2";
            //Act
            var mockHorario = new Mock<IServices>();
            //Mediante mock introducimos el return del metodo en el servicio
            mockHorario.Setup(mh => mh.Nulo(It.IsAny<String>())).Returns(true);
            HorarioBOL HorarioService = new HorarioBOL(mockHorario.Object);
            //Resultado de la ejecución del codigo
            var resultado = HorarioService.EliminarHorario(Codigo);
            //Assert
            Assert.AreEqual("Horario no existe", resultado[0]);

        }
        /// <summary>
        /// TSC-011
        /// Requisito 04
        /// </summary>
        [Test]
        public void EliminarHorarioConUnCodigoCorrecto()
        {
            //Arrange
            String Codigo = "02";
            //Act
            var mockHorario = new Mock<IServices>();
            //Mediante mock introducimos el return del metodo en el servicio
            mockHorario.Setup(mh => mh.Nulo(It.IsAny<String>())).Returns(true);
            HorarioBOL HorarioService = new HorarioBOL(mockHorario.Object);
            //Resultado de la ejecución del codigo
            var resultado = HorarioService.EliminarHorario(Codigo);
            //Assert
            Assert.AreEqual("Eliminación de horario con exito", resultado[0]);

        }

        /// <summary>
        /// TSC-012
        /// Requisito 04
        /// </summary>
        [Test]
        public void EliminarHorarioConUnCodigoNulo()
        {
            //Arrange
            String Codigo = "";
            //Act
            var mockHorario = new Mock<IServices>();
            //Mediante mock introducimos el return del metodo en el servicio
            mockHorario.Setup(mh => mh.Nulo(It.IsAny<String>())).Returns(false);
            HorarioBOL HorarioService = new HorarioBOL(mockHorario.Object);
            //Resultado de la ejecución del codigo
            var resultado = HorarioService.EliminarHorario(Codigo);
            //Assert
            Assert.AreEqual("Codigo esta en nulo", resultado[0]);

        }


        /// <summary>
        /// TSC-013
        /// Requisito 05
        /// </summary>

        [Test]
        public void GenerarReporteCodigoCorrecto()
        {
            //Arrange
            String Codigo = "01";
            //id del elemento que se esta estrayendo de la base de datos
            HorarioModel horario = new HorarioModel
            {
                Id  = 2,
                CodigoHorario = "01",
                NumeroBus = "ZZA400",
                Descripcion = "Horarios de la Palmera",
                TiempoSalida = "7:00",
                TiempoLlegada = "8:00",
                Estado = true
            };
            hbol = new HorarioBOL();
            //Act
            //El horario completo para el reporte
            HorarioModel result = (HorarioModel)hbol.Reporte(Codigo)[0];
            //Assert
            Assert.AreEqual(horario.Id,result.Id);
            Assert.AreEqual(horario.CodigoHorario, result.CodigoHorario);
            Assert.AreEqual(horario.NumeroBus, result.NumeroBus);
        }

        /// <summary>
        /// TSC-014
        /// Requisito 05
        /// </summary>
        [Test]
        public void GenerarReporteConCodigoIncorrecto()
        {
            //Arrange
            String Codigo = "123 rt";
            String resultado = "Codigo no existe";
            //Act
            hbol = new HorarioBOL();
            var result = hbol.Reporte(Codigo);
            //Assert
            Assert.AreEqual(resultado, result[0]);
        }

        /// <summary>
        /// TSC-015
        /// Requisito 05
        /// </summary>
        [Test]
        public void GenerarReporteConCodigoNulo()
        {
            //Arrange
            String Codigo = "";
            String resultado = "No puede contener espacios en blanco o nulos";
            //Act
            hbol = new HorarioBOL();
            var result = hbol.Reporte(Codigo);
            //Assert
            Assert.AreEqual(resultado, result[0]);
        }
    }
}
