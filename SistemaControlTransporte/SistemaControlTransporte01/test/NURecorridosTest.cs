﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;
using SistemaControlTransporte.src.BOL;
using SistemaControlTransporte.src.Models;
using SistemaControlTransporte01.src;

namespace SistemaControlTransporte.test
{
    [TestFixture]
    public class NURecorridosTest
    {
        /// <summary>
        /// TSC-016
        /// Requisito 06
        /// </summary>
        [Test]
        public void CrearRecorridoCorrecto()
        {
            //Arrange
            RecorridoModel r = new RecorridoModel
            {
                CodigoRecorrido = "002",
                NumeroBus = "ZZA400",
                Descripcion = "CQ - SJ",
                TiempoDemora = 4,
                ZonaRecorrido = "02",
                CantidadRecorridos = 1,
                Estado = true
            };
            //Act
            var MockRecorrido = new Mock<IServices>();
            //Mediante mock introducimos el return del metodo en el servicio
            MockRecorrido.Setup(mh => mh.RevisarRecorrido(It.IsAny<RecorridoModel>())).Returns(new ArrayList { "Datos Correctos" });
            RecorridoBOL RecorridoService = new RecorridoBOL(MockRecorrido.Object);
            var resultado = RecorridoService.CrearRecorrido(r);
            //Assert
            Assert.AreEqual("Realizado", resultado[0]);

        }
        /// <summary>
        /// TSC-017
        /// Requisito 06
        /// </summary>
        [Test]
        public void CrearRecorridoConUnNumeroDeBusQueNoExiste()
        {
            //Arrange
            RecorridoModel r = new RecorridoModel
            {
                CodigoRecorrido = "004",
                NumeroBus = "45234fw",
                Descripcion = "CQ - SJ",
                TiempoDemora = 4,
                ZonaRecorrido = "02",
                CantidadRecorridos = 1,
                Estado = true
            };
            //Act
            var MockRecorrido = new Mock<IServices>();
            //Mediante mock introducimos el return del metodo en el servicio
            MockRecorrido.Setup(mh => mh.RevisarRecorrido(It.IsAny<RecorridoModel>())).Returns(new ArrayList { "Numero de bus no existe" });
            RecorridoBOL RecorridoService = new RecorridoBOL(MockRecorrido.Object);
            var resultado = RecorridoService.CrearRecorrido(r);
            //Assert
            Assert.AreEqual("El numero de bus no esta en la base de datos", resultado[0]);

        }
        /// <summary>
        /// TSC-018
        /// Requisito 06
        /// </summary>
        [Test]
        public void CrearRecorridoConDatosNulos()
        {
            //Arrange
            RecorridoModel r = new RecorridoModel
            {
                CodigoRecorrido = "004",
                NumeroBus = "",
                Descripcion = "",
                TiempoDemora = 4,
                ZonaRecorrido = "02",
                CantidadRecorridos = 1,
                Estado = true
            };

            //Act
            var MockRecorrido = new Mock<IServices>();
            //Mediante mock introducimos el return del metodo en el servicio
            MockRecorrido.Setup(mh => mh.RevisarRecorrido(It.IsAny<RecorridoModel>())).Returns(new ArrayList { "Tiene datos vacios" });
            RecorridoBOL RecorridoService = new RecorridoBOL(MockRecorrido.Object);
            var resultado = RecorridoService.CrearRecorrido(r);
            //Assert
            Assert.AreEqual("Tiene datos vacios", resultado[0]);
        }

        /// <summary>
        /// TSC-019
        /// Requisito 07
        /// </summary>
        [Test]
        public void BuscarRecorridoCorrecto()
        {
            //Arrange
            string CodigoRecorrido = "002";
            RecorridoBOL rbol = new RecorridoBOL();
            string resultaesperado = "Se encontró un recorrido.";
            //Act
            var result = rbol.BuscarRecorrido(CodigoRecorrido);
            //Assert
            Assert.AreEqual(resultaesperado, result[0]);
        }

        /// <summary>
        /// TSC-020
        /// Requisito 07
        /// </summary>
        [Test]
        public void BuscarRecorridoCaracteresEsp()
        {
            //Arrange
            string CodigoRecorrido = "65??";
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "No se aceptan caracteres especiales en el codigo, solo números y letras.";
            //Act
            var result = rbol.BuscarRecorrido(CodigoRecorrido);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);

        }

        /// <summary>
        /// TSC-021
        /// Requisito 07
        /// </summary>
        [Test]
        public void BuscarRecorridoEmpty()
        {
            //Arrange
            string CodigoRecorrido = "";
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "No se aceptan espacios en blanco o valores nulos para la busqueda de recorridos.";
            //Act
            var result = rbol.BuscarRecorrido(CodigoRecorrido);
            //Assert
            Assert.AreEqual(resultadoesperado, result[0]);

        }

        /// <summary>
        /// TSC-022
        /// Requisito 08
        /// </summary>
        [Test]
        public void ActualizarSinNB()
        {
            //Arrange
            RecorridoModel r = new RecorridoModel
            {
                CodigoRecorrido = "001",
                NumeroBus = " ",
                Descripcion = "CQ - Heredia",
                TiempoDemora = 3,
                ZonaRecorrido = "01",
                CantidadRecorridos = 2,
                Estado = true
            };
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "Es necesario asignar un número de bus, no se aceptan espacios en blanco o valores nulos.";
            //Act
            var result = rbol.ActualizarRecorrido(r);
            //Assert
            Assert.AreEqual(resultadoesperado, result);

        }
        /// <summary>
        /// TSC-023
        /// Requisito 08
        /// </summary>
        [Test]
        public void ActualizarRecorridoSinCD()
        {
            //Arrange
            RecorridoModel r = new RecorridoModel
            {
                CodigoRecorrido = "",
                NumeroBus = "S001CA",
                Descripcion = "CQ - Heredia",
                TiempoDemora = 3,
                ZonaRecorrido = "01",
                CantidadRecorridos = 2,
                Estado = true
            };
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "Se necesita un código de recorrido.";
            //Act
            var result = rbol.ActualizarRecorrido(r);
            //Assert
            Assert.AreEqual(resultadoesperado, result);

        }
        /// <summary>
        /// TSC-024
        /// Requisito 08
        /// </summary>
        [Test]
        public void ActualizarRecorridoExito()
        {
            //Arrange
            RecorridoModel r = new RecorridoModel
            {
                CodigoRecorrido = "001",
                NumeroBus = "S001CA",
                Descripcion = "CQ - Heredia",
                TiempoDemora = 3,
                ZonaRecorrido = "01",
                CantidadRecorridos = 2,
                Estado = true
            };
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "Recorrido actualizado de manera exitosa.";
            //Act
            var result = rbol.ActualizarRecorrido(r);
            //Assert
            Assert.AreEqual(resultadoesperado, result);

        }

        /// <summary>
        /// TSC-025
        /// Requisito 09
        /// </summary>
        [Test]
        public void EliminarSinCodigo()
        {
            //Arrange
            string codigo = "  ";
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "Se necesita un código de recorrido, no se aceptan espacios en blanco o valores nulos.";
            //Act
            var result = rbol.EliminarRecorrido(codigo);
            //Assert
            Assert.AreEqual(resultadoesperado, result);
        }
        /// <summary>
        /// TSC-026
        /// Requisito 09
        /// </summary>
        [Test]
        public void EliminarCodigoNoRegistrado()
        {
            //Arrange
            string codigo = "4564";
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "El código de recorrido proporcionado no pertenece a ningún recorrido existente.";
            //Act
            var result = rbol.EliminarRecorrido(codigo);
            //Assert
            Assert.AreEqual(resultadoesperado, result);

        }
        /// <summary>
        /// TSC-027
        /// Requisito 09
        /// </summary>
        [Test]
        public void EliminarRecorridoExito() {
            //Arrange
            string codigo = "003";
            RecorridoBOL rbol = new RecorridoBOL();
            string resultadoesperado = "El recorrido se eliminó de manera exitosa.";
            //Act
            var result = rbol.EliminarRecorrido(codigo);
            //Assert
            Assert.AreEqual(resultadoesperado, result);

        }

        /// <summary>
        /// TSC-028
        /// Requisito 10
        /// </summary>
        [Test]
        public void ReporteRecorridoExitoso()
        {
            //Arrange
            RecorridoBOL recorrido = new RecorridoBOL();
            string codigoReccorrido = "002";
            string mensajeResultado = "Se ha producido el reporte.";
            //Act
            ArrayList result = recorrido.ReporteRecorrido(codigoReccorrido);
            //Assert
            Assert.AreEqual(mensajeResultado, result[0]);

        }

        /// <summary>
        /// TSC-029
        /// Requisito 10
        /// </summary>
        [Test]
        public void ReporteRecorridoNuloVacio()
        {
            //Arrange
            RecorridoBOL recorrido = new RecorridoBOL();
            string codigoReccorrido = " ";
            string mensajeResultado = "Es necesario asignar numero de ruta, no se aceptan espacios en blanco o valores nulos.";
            //Act
            ArrayList result = recorrido.ReporteRecorrido(codigoReccorrido);
            //Assert
            Assert.AreEqual(mensajeResultado, result[0]);
        }

        /// <summary>
        /// TSC-030
        /// Requisito 10
        /// </summary>
        [Test]
        public void ReporteRecorridoCaracteresEspeciales()
        {
            //Arrange
            RecorridoBOL recorrido = new RecorridoBOL();
            string codigoReccorrido = "001-AAA";
            string mensajeResultado = "No se aceptan caracteres especiales en el codigo, solo números y letras.";
            //Act
            ArrayList result = recorrido.ReporteRecorrido(codigoReccorrido);
            //Assert
            Assert.AreEqual(mensajeResultado, result[0]);

        }
    }
}
